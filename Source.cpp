#include <iostream>
#include <locale>
#include <string>
#include <cstdlib>
#include <ctime>
#include <vector>
#include <iomanip>

using namespace std;   

//aliohjelma vapaan huoneen tarkistukselle
bool huoneTarkistaja(int huonenumero, int huoneetTaulukko) {
    return (huonenumero == huoneetTaulukko);        //palauttaa true jos huonenumero l�ytyy huonetaulukosta
}

//aliohjelma varausnumeron luomiselle
int luoVarausnumero() {
    srand(time(0));
    int varausnumero = (rand() % 90000) + 10000;    //arpoo varausnumeron v�lilt� 10000 - 99999
    return varausnumero;                            //palauttaa varausnumeron
}

//aliohjelma hinnan alennuksen laskemiselle
double alennusArpoja() {
    srand(time(0));
    double x = rand() % 3;
    double y = 1 - (x / 10);
    return y;
}


//aliohjelma joka luo huonemm��r�n satunnaisesti
int luoHuoneMaara() {
    srand(time(0));
    int koko = (rand() % 261) + 40;                 //arvotaan huonem��r� v�lilt� 40 - 300
    if (koko % 2 != 0) {                            //jos koko ei ole tasaluku lis�t��n 1
        koko += 1;                                  
    }
    return koko;                                    //palautetaan huonem��r�
}

//aliohjelma joka arpoo huonenumeron jos asiakas ei halua itse valita
int huonenumero(int huonekoko, int huonemaara) {
    srand(time(0));
    int huonenumero = 0;
    if (huonekoko == 1) {                           //yhden henkil�n huoneet
        huonenumero = (rand() % (huonemaara / 2) + 1) + 1;  //v�lilt� 1 - puolet huonem��r�st�
    }

    if (huonekoko == 2) {                           //kahden henkil�n huoneet
        huonenumero = (rand() % (huonemaara + 1) + (huonemaara / 2)) + 1; //v�lilt� puolet huoneista - max huonem��r�
    }
    return huonenumero;                             //palauttaa arvotun huonenumeron
}


//p��ohjelma
int main() {
    setlocale(LC_ALL, "fi_FI");

    struct varaus {                 //tietue asiakastiedoille
        string nimi;                //asiakkaan nimi
        int puhelinnumero;          //asiakkaan puhelinnumero
        int varausnumero;           //asiakkaan varausnumero
        int varatut[300];           //varatut huoneet s�il�t��n t�ss� taulukossa
        int yhenYot = 0;            //yhden henkil� huoneiden y�m��r�t
        int kahenYot = 0;           //kahden henkil�n huoneiden y�m��r�t
        int hinta;                  //varauksen hinta
        double alennuskerroin;             //alennuskerroin
        int alennettu;              //varauksen hinta alennuksella
    };
vector<varaus> varaukset;           //vektori jossa s�ilytet��n varauksia jos niit� tehd��n monia
int varausnumero = 0;               //initioidaan varausnumero
int huonemaara = luoHuoneMaara();   //luodaan hotellin huonem��r�

int huoneet[300]{};                 //luodaan taulukko vapaille huoneille ja t�ytet��n se
    for (int i = 0; i < huonemaara; i++) {
        huoneet[i] = i + 1;
    }

    while (true) {                                  //tulostetaan valikko asiakkaalle
        int valinta;
        cout << "Valikko:" << endl;
        cout << "1. Huoneiden hinnat" << endl;      //1. tulostaa huoneiden hinnat
        cout << "2. Tee uusi varaus" << endl;       //2. uuden varauksen luominen
        cout << "3. Vapaat huoneet" << endl;        //3. tulostaa kaikkien vapaiden huoneiden numerot
        cout << "4. Tarkastele varauksia" << endl;  //4. hakukone varauksille
        cout << "5. Poista varaus" << endl;         //5. varausten poisto
        cout << "6. Lopeta" << endl;                //6. lopeta ohjelma
        cout << "Valitse toiminto (1-6): ";
        cin >> valinta;
        cout << endl;

        if (!cin || valinta < 1 || valinta > 6) {   //sy�tteentarkistus valikon sy�t�lle, v�lilt� 1 - 6, ei saa olla aakkonen
            cout << "Virheellinen valinta. Uusi valinta: ";
            cout << endl;
            cin.clear();
            cin.ignore(numeric_limits<streamsize>::max(), '\n');
            continue;
        }

        //valikon toiminnot jaettuna switch komennolla
        switch (valinta) {
        case 1://jos asiakas haluaa n�hd� huonehinnat tulostetaan ne
            cout << "Yhden henkil�n huone maksaa EUR 100 y�lt�." << endl;
            cout << "Kahden henkil�n huone maksaa EUR 150 y�lt�." << endl << endl;
            break;

        case 2://huoneen varaus

            while (true) { //jos asiakas vastaa k tehd��n kokonaan uus varaus, e lopettaa varausten tekemisen
                cout << "Haluatko tehd� uuden varauksen? k/e : ";
                char vastaus;
                cin >> vastaus;
                cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
                cout << endl;

                while (vastaus != 'k' && vastaus != 'e') { //sy�tteentarkistus muut merkit kuin k ja e ei kelpaa
                    cout << "Virheellinen valinta." << endl << "Haluatko tehd� uuden varauksen? k/e : ";
                    cin >> vastaus;
                    cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
                    cout << endl;
                }
                if (vastaus == 'k') {   //asiakas haluaa tehd� uuden varauksen
                    varaus v;           //luodaan tietueen avulla varaukselle oma muisti

                    for (int i = 0; i < 300; i++) { //alustetaan huonevaraus taulukko nollilla
                        v.varatut[i] = 0;
                    }
                    cout << "Anna varaajan nimi (etunimi sukunimi): "; //varaajan nimen kysely
                    getline(cin, v.nimi);
                    cout << endl;

                    cout << "Anna varaajan puhelinnumero: "; //varaajan numeron kysely
                    cin >> v.puhelinnumero;
                    cout << endl;

                    while (cin.fail()) { //sy�tteentarkistus muut merkit kuin numerot ei kelpaa
                        cin.clear();
                        cin.ignore(numeric_limits<streamsize>::max(), '\n');
                        cout << "virheellinen numero, anna varaajan puhelinnumero: ";
                        cin >> v.puhelinnumero;
                        cout << endl;
                    }
                    while (true) {
                        cout << "Varataanko yhden vai kahden hengen huone?(L=lopeta lis�ys) 1/2/L: "; //lis�t��nk� huone varaukseen
                        cin >> vastaus;
                        cin.clear();
                        cin.ignore(numeric_limits<streamsize>::max(), '\n');
                        cout << endl;

                        while (vastaus != '1' && vastaus != '2' && vastaus != 'L') {  //sy�tteentarkistus, sy�tteen pit�� olla 1, 2 tai L
                            cout << "Virheellinen valinta. Valitse 1, 2 tai L: ";
                            cin >> vastaus;
                            cin.clear();
                            cin.ignore(numeric_limits<streamsize>::max(), '\n');
                            cout << endl;
                        }

                        if (vastaus == '1') {  //yhden henkil�n huoneen lis�ys
                            cout << "Montako y�t� huone varataan: ";
                            int yot;
                            cin >> yot;
                            cin.clear();
                            cin.ignore(numeric_limits<streamsize>::max(), '\n');
                            cout << endl;

                            while (cin.fail() || yot < 1) {//sy�tteentarkastus ei saa olla aakkonen, ei anneta tilata huonetta alle yhdeksi y�ksi
                                cin.clear();
                                cin.ignore(numeric_limits<streamsize>::max(), '\n');

                                cout << "Kerro y�m��r� numerona, varaa v�hint��n 1 y�: ";
                                cin >> yot;
                                cout << endl;
                            }
                            v.yhenYot += yot;  //lis�t��n yhden henkil�n huoneen y�t muistiin

                            cout << "Haluatko (v)alita huonenumeron vai (a)rvotaanko se? v/a: ";//kysyt��n huonenumeron arpominen tai valinta
                            cin >> vastaus;
                            cin.clear();
                            cin.ignore(numeric_limits<streamsize>::max(), '\n');
                            cout << endl;
                            
                            while (vastaus != 'v' && vastaus != 'a') {//sy�tteentarkastus muu kuin v/a ei kelpaa
                                cout << "V��r� valinta." << endl << endl << "Haluatko (v)alita huonenumeron vai (a)rvotaanko se? v/a: ";
                                cin >> vastaus;
                                cin.clear();
                                cin.ignore(numeric_limits<streamsize>::max(), '\n');
                                cout << endl;
                            }

                            if (vastaus == 'v') { //jos asiakas haluaa valita huonenumeron
                                cout << "valitse huonenumero v�lilt� 1 - " << huonemaara / 2 << ": ";
                                int huoneNro = 0;
                                cin >> huoneNro;
                                cin.clear();
                                cin.ignore(numeric_limits<streamsize>::max(), '\n');
                                cout << endl;

                                //sy�tteentarkastus
                                while (cin.fail()                                           //aakkoset
                                    || huoneNro < 1                                         //v�hint��n min huonenumero       
                                    || huoneNro >(huonemaara / 2)                           //enint��n max huonenumero 
                                    || !huoneTarkistaja(huoneNro, huoneet[huoneNro - 1]))   //onko huoneNro huone taulukossa,
                                                                                            //jos on palautuu true huone on taulukossa     
                                {                                                           //k��net��n falseksi koska huone vapaa
                                    if (!huoneTarkistaja(huoneNro, huoneet[huoneNro - 1])) {    //tarkastetaan onko huone varattu
                                        cout << "Huone on varattu." << endl;                    //ilmoitetaan asiakkaalle jos huone on varattu
                                    }
                                    cout << "Huonenumeron pit�� olla numero v�lilt� 1 - " << huonemaara / 2 << ": ";
                                    cin >> huoneNro;
                                    cout << endl;
                                }
                                huoneet[huoneNro - 1] = 0; //poistetaan huonenumero vapaiden numeroiden taulukosta
                                v.varatut[huoneNro - 1] = huoneNro;
                            }
                            if (vastaus == 'a') {//arvotaan huonenumero
                                int huoneNro = huonenumero(1, huonemaara);
                                while (!huoneTarkistaja(huoneNro, huoneet[huoneNro - 1])) {//toistetaan kunnes arvotaan vapaa huone
                                    huoneNro = huonenumero(1, huonemaara);

                                }
                                cout << "Huonenumerosi on: " << huoneNro << endl;//ilmoitetaan arvottu numero
                                huoneet[huoneNro - 1] = 0; //poistetaan huonenumero vapaiden numeroiden taulukosta
                                v.varatut[huoneNro - 1] = huoneNro;
                            }
                        }
                        if (vastaus == '2') {  //jos halutaan lis�t� 2hh
                            cout << "Montako y�t� huone varataan: ";
                            int yot = 0;
                            cin >> yot;
                            cout << endl;

                            while (cin.fail() || yot < 1) {//sy�tteentarkastus aakkoset, ja ei anneta tilata huonetta alle yhdeksi y�ksi
                                cin.clear();
                                cin.ignore(numeric_limits<streamsize>::max(), '\n');

                                cout << "Y�m��r�n pit�� olla numero joka on suurempi kuin 0: ";
                                cin >> yot;
                                cout << endl;
                            }
                            v.kahenYot += yot;   //lis�t��n kahden henkil�n huoneen y�t muistiin

                            cout << "Haluatko (v)alita huonenumeron vai (a)rvotaanko se? v/a: ";
                            cin >> vastaus;
                            cout << endl;

                            //sy�tteentarkastus
                            while (vastaus != 'v' && vastaus != 'a') {
                                cout << "V��r� valinta." << endl << endl << "Haluatko (v)alita huonenumeron vai (a)rvotaanko se? v/a: ";
                                cin >> vastaus;
                                cout << endl;
                            }

                            if (vastaus == 'v') {
                                cout << "Valitse huonenumero v�lilt� " << huonemaara / 2 + 1 << " - " << huonemaara << ": ";
                                int huoneNro = 0;
                                cin >> huoneNro;
                                cout << endl;

                                //sy�tteentarkastus
                                while (cin.fail()                                           //aakkoset
                                    || huoneNro < huonemaara / 2 + 1                        //min huonenumero       
                                    || huoneNro > huonemaara                                //max huonenumero 
                                    || !huoneTarkistaja(huoneNro, huoneet[huoneNro - 1]))   //onko huoneNro huone taulukossa,
                                                                                            //jos on palautuu true huone on taulukossa     
                                {                                                           //k��net��n falseksi koska huone vapaa
                                    cin.clear();
                                    cin.ignore(numeric_limits<streamsize>::max(), '\n');
                                    //jos saadaan tarkastuksesta ett� huonetta ei l�ydy vapaiden huoneiden taulukosta, saadaan false. k��net��n se
                                    if (!huoneTarkistaja(huoneNro, huoneet[huoneNro - 1])) {
                                        cout << "Huone on varattu." << endl;                    //ilmoitetaan asiakkaalle
                                    }
                                    //kysyt��n huoneNro uudelleen
                                    cout << "Huonenumeron pit�� olla numero v�lilt� " << huonemaara / 2 + 1 << " - " << huonemaara << ": ";
                                    cin >> huoneNro;
                                    cout << endl;
                                }
                                //poistetaan huonenumero vapaiden huonenumeroiden taulukosta
                                huoneet[huoneNro - 1] = 0;
                                v.varatut[huoneNro - 1] = huoneNro;
                            }
                            if (vastaus == 'a') {                                           //arvotaan huonenumero
                                int huoneNro = huonenumero(2, huonemaara);                  //aliohjelmaa k�ytt�en

                                while (!huoneTarkistaja(huoneNro, huoneet[huoneNro - 1])) { //jos huone ei ole vapaa toistetaan
                                    huoneNro = huonenumero(2, huonemaara);

                                }
                                cout << "Huonenumerosi on: " << huoneNro << endl;
                                //poistetaan huonenumero vapaiden numeroiden taulukosta
                                huoneet[huoneNro - 1] = 0;
                                v.varatut[huoneNro - 1] = huoneNro;
                            }
                        }
                        if (vastaus == 'L') { // jos lopetetaan huoneiden lis�ys
                            v.varausnumero = luoVarausnumero(); //luodaan varausnumero aliohjelmalla
                            cout << "Varausnumerosi on: " << v.varausnumero << endl; //kerrotaan varausnumero
                            v.hinta = (v.yhenYot * 100) + (v.kahenYot * 150); //lasketaan hinta
                            v.alennuskerroin = alennusArpoja();               //arvotaan alennus %
                            v.alennettu = v.alennuskerroin * v.hinta;         //lasketaan alennettu hinta
                            varaukset.push_back(v); //varaus lopetetaan, varauksen tiedot ty�nnet��n vektoriin
                            break;
                        }
                    }
                }
                if (vastaus == 'e') {//ei haluta luoda uutta varausta, menee takaisin valikkoon.
                    break;
                }
            };
            break;

            //vapaat huoneet
        case 3:
            cout << endl << "Yhden henkil�n huoneet: " << endl; //tulostetaan vapaat 1 hh
            for (int i = 0; i < (huonemaara/2); i++) {
                if (huoneet[i] > 0) 
                    cout << huoneet[i] << " ";
            }
            cout << endl << endl;
            
            cout << "Kahden henkil�n huoneet: " << endl;        //tulostetaan vapaat 2hh
            for (int i = (huonemaara/2); i < huonemaara; i++) {
                if(huoneet[i] > 0)
                cout << huoneet[i] << " ";
            }
            break;

            //tarkastele varauksia
        case 4:
            cout << "Anna haettavan varauksen varausnumero: " << endl;                      //tulostetaan varauksen tiedot varausnumeron perusteella
            cin >> varausnumero;
            cout << endl;

            for (const varaus& v : varaukset) {
                if (v.varausnumero == varausnumero) {
                    cout << setw(25) << "Varausnumero" << setw(20) << v.varausnumero << endl;
                    cout << setw(25) << "Nimi" << setw(20) << v.nimi << endl;
                    cout << setw(25) << "Puhelinnumero" << setw(20) << v.puhelinnumero << endl;
                    cout << setw(25) << "Hinta ilman alennusta" << setw(20) << v.hinta << endl;
                    cout << setw(25) << "Alennusprosentti" << setw(19) << 100 - (100 * v.alennuskerroin) << "%" << endl; //muutetaan alennuskerroin prosentti muotoon
                    cout << setw(25) << "Hinta alennuksella" << setw(20) << v.alennettu << endl;
                    cout << setw(25) << "Varatut huoneet ovat: ";
                    for (int i = 0; i < 300; i++) { //tulostetaan varauksessa olevat huonenumerot
                        if (v.varatut[i] > 0) {
                            cout << v.varatut[i] << " ";
                        }
                    }
                }
            }
            cout << endl << endl;
            break;
        case 5:                                                         //varauksen poisto
            cout << "Anna poistettavan varauksen varausnumero: ";       //varausnumeron perusteella
            cin >> varausnumero;
            for (const varaus& v : varaukset) {                         //lis�t��n ensin huoneet takaisin vapaiden huoneiden taulukkoon
                if (v.varausnumero == varausnumero) {
                    for (int i = 0; i < 300; i++) {
                        if (v.varatut[i] > 0) {
                            huoneet[i] = v.varatut[i];
                        }
                    }
                }
            }
            for (auto i = varaukset.begin(); i != varaukset.end(); ++i) { //poistetaan varaus varaus vektorista
                if (i->varausnumero == varausnumero) {
                    varaukset.erase(i);
                    break;

                }
            }
            cout << endl;
            break;

            //ohjelman sulku
        case 6:
            cout << "Kiitos k�yt�st�. Ohjelma suljetaan." << endl;
            return 0;
        }
    }

    return 0;
}